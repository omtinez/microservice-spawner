import { MicroService } from 'microservice';
import * as minimist from 'minimist';
import * as path from 'path';
import * as url from 'url';
import { SpawnedProcess, run, load } from './cmd';

// Main entrypoint
export function main(opts : any = {}): MicroService {

    // Parse command-line arguments
    const args = minimist(process.argv.slice(1));

    // Helper function used to read arguments
    function getopt(name: string) {
        return opts[name] || args[name];
    }

    // Initialize microservice
    const service = new MicroService(getopt('db'));

    // Helper function used to spawn a process given its npm package name
    function spawn(package_name: string, run_args: any, keepalive: boolean) {
        return new Promise<number>((resolve, reject) => {

            // Define binary names and arguments
            const npm_bin = 'npm';
            const npm_args = ['install', '--prefix', __dirname, package_name];

            run(npm_bin, npm_args).then(() => {
                // Log success and proceed
                const msg = `Package ${package_name} successfully installed`;
                service.log('V', msg);
                if (run_args.orchestrator || getopt('orchestrator')) {
                    run_args.orchestrator = run_args.orchestrator || getopt('orchestrator');
                }
                //require(request.query.package_name).main(run_args);
                const package_path = package_name.split('/').pop();
                const module_path = path.join(__dirname, 'node_modules', package_path, 'dist', 'index.js');
                const run_args_ = Object.keys(run_args).reduce((acc, key) => {
                    return acc.concat([`--${key}`, run_args[key]]);
                }, []);
                service.log('V', module_path, run_args_);
                return load(module_path, run_args_);

            }).then(spawned => {
                // Add the spawned process to database
                service.db.run(`
                    INSERT INTO spawned (package_name, module_path, args, pid, keepalive) 
                    VALUES (?, ?, ?, ?, ?)`,
                    [package_name, spawned.module, JSON.stringify(run_args), spawned.pid, keepalive], err => {
                        if (err) {
                            reject(err);
                        } else {
                            if (keepalive) {
                                spawned.proc.on('exit', function exitHandler(code, signal) {
                                    const msg = `Process ${spawned.module} [${spawned.pid}] died`;
                                    service.log('E', msg);
                                    const old_pid = spawned.proc.pid;
                                    load(spawned.module, spawned.args).then(spawned => {
                                        spawned.proc.on('exit', exitHandler);
                                        service.db.run(`UPDATE spawned SET pid = ? WHERE pid = ?`,
                                        [spawned.pid, old_pid], err => {
                                            if (err) {
                                                const msg = `Error updating pid [${old_pid} -> ${spawned.pid} for ${package_name}`;
                                                service.log('E', msg, err.message);
                                            }
                                        })
                                    });
                                })
                            }

                            // Return the PID and success message
                            const pid = spawned.pid;
                            const msg = `Successfully spawned MicroService "${package_name}"`;
                            service.log('I', msg, `[${pid}]`);
                            resolve(pid);
                        }
                });
            }).catch(err => reject(err));
        });
    }

    // Global objects used to keep track of running processes statistics
    //const global_procs: {[key: string]: SpawnedProcess[]} = {};
    const global_stats: {[key: string]: any} = {};

    // Initialize services database
    service.db.serialize(() => {
        service.db.run(`
            CREATE TABLE IF NOT EXISTS spawned (
                package_name      TEXT NOT NULL,
                module_path       TEXT NOT NULL,
                args              TEXT NOT NULL,
                pid               INTEGER NOT NULL,
                keepalive         INTEGER DEFAULT 1,
                timestamp         DATETIME DEFAULT CURRENT_TIMESTAMP)`);

        // Spawned processes if there were any left to start
        service.db.each(`
            SELECT package_name, module_path, args, pid FROM spawned WHERE keepalive = 1`,
            (err, row) => {
                if (err || !row) {
                    const msg = 'Error querying database';
                    service.log('E', msg, err.message);
                } else {
                    spawn(row.package_name, JSON.parse(row.args), true).then(pid => {
                        service.db.run(`DELETE FROM spawned WHERE pid = ?`, row.pid);
                        const msg = `Service for ${row.package_name} successfully restarted [${pid}]`;
                        service.log('I', msg);
                    }).catch(err => {
                        // Nothing we can do here...
                    });
                }
        });
    });

    // Routes setup
    service.route('/status', (request, response) => {
        response.send({status: 'OK', data: global_stats});
    });

    service.route('/spawn', (request, response) => {
        // Helper function used to catch promise rejections and return
        const catcher = (err: Error) => {
            const msg = `Error spawning process`;
            response.status(500).send(msg);
            service.log('E', msg, err.message);
        }

        const run_args = JSON.parse(request.query.run_args || '{}');
        const keepalive = !request.query.hasOwnProperty('keepalive') || !!request.query.keepalive;
        spawn(request.query.package_name, run_args, keepalive).then(pid => {
            const msg = `Successfully spawned MicroService "${request.query.package_name}"`;
            response.send({status: 'OK', data: {message: msg, pid: pid}});
        }).catch(err => {
            const msg = `Error spawning process`;
            response.status(500).send(msg);
            service.log('E', msg, err.message);
        });
    });

    service.route('/kill', (request, response) => {
        service.db.get(`
            SELECT package_name, module_path, args, pid, keepalive FROM spawned
            WHERE pid = ?`, [request.query.pid], (err, row) => {
                if (err) {
                    const msg = `Error retrieving from database`;
                    service.log('E', msg, err.message);
                    response.status(500).send(msg);
                } else if (!row) {
                    const msg = `Process with PID ${request.query.pid} not found`;
                    service.log('E', msg);
                    response.status(400).send(msg);
                } else {
                    process.kill(row.pid);
                    service.db.run(`DELETE FROM spawned WHERE pid = ?`, [request.query.pid], err => {
                        if (err) {
                            const msg = 'Error deleting from database';
                            service.log('E', msg, err.message);
                            response.status(500).send(msg);
                        } else {
                            const msg = `Process ${row.pid} belonging to ${row.package_name} successfully killed`;
                            response.send({status: 'OK', data: msg});
                        }
                    });
                }
        });
    }, {method: 'GET', mandatoryQueryParameters: ['pid']});

    // Collect status statistics on an interval
    setInterval(() => {
        service.db.all(`
            SELECT message FROM log WHERE level = 'E' ORDER BY timestamp DESC LIMIT 10`, (err, rows) => {
                global_stats.last_errors = rows ? rows.map((row: any) => row.message) : [];
                service.db.all(
                    `SELECT package_name, args, pid, keepalive FROM spawned`, (err, rows) => {
                        global_stats.spawned_processes = rows || [];
                });
        });
    }, getopt('interval') || 60000)

    // Pick a random port
    service.start(parseInt(getopt('port')) || Math.floor(Math.random() * 8999) + 1000);

    // Register with orchestrator if option was passed as argument
    if (getopt('orchestrator')) {
        const orchestrator_url = url.parse(getopt('orchestrator'));
        service.register(orchestrator_url, 'spawner', {local: '/', remote: '/spawn'});
    }

    return service;
}

// If starting from the command line, begin execution
if (typeof require !== 'undefined' && require.main === module) {
    main();
}
