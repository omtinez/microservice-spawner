"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const child_process_1 = require("child_process");
const which = require("which");
function run(bin, args) {
    return new Promise((resolve, reject) => {
        which(bin, (err, binpath) => {
            if (err) {
                reject(new Error(`"${bin}" could not be found.`));
            }
            else {
                const child = child_process_1.spawn(bin, args, { shell: true, stdio: ['ipc'], cwd: __dirname });
                child.on('close', code => {
                    if (code !== 0) {
                        reject(new Error(`"${bin}" exited with non-zero code ${code}`));
                    }
                    else {
                        resolve();
                    }
                });
            }
        });
    });
}
exports.run = run;
function load(module, args, timeout = 15000) {
    return new Promise((resolve, reject) => {
        let finished = false;
        let spawned;
        const child = child_process_1.fork(module, args, { stdio: [0, 1, 2, 'ipc'], cwd: __dirname });
        child.on('error', err => {
            if (!finished) {
                finished = true;
                reject(err);
            }
        });
        child.on('disconnect', () => {
            if (!finished) {
                finished = true;
                spawned = {
                    proc: child,
                    module: module,
                    pid: child.pid,
                    args: args,
                    timestamp: new Date().getTime()
                };
                resolve(spawned);
            }
        });
        child.on('close', code => {
            if (!finished) {
                finished = true;
                reject(new Error(`"${module}" exited prematurely with code ${code}`));
            }
        });
        setTimeout(() => {
            if (!finished) {
                finished = true;
                child.kill();
                return reject(new Error(`"${module}" timed out before signaling start so it was killed`));
            }
        }, timeout);
    });
}
exports.load = load;
