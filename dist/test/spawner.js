"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const assert = require("assert");
const url = require("url");
const request = require("supertest");
const requests = require("request");
const index_1 = require("../index");
const service = index_1.main();
describe('routes', () => {
    beforeEach(() => {
        service.stop();
        service.clearCache(true);
        service.start(Math.floor(Math.random() * 8999) + 1000);
    });
    afterEach(() => {
        service.stop();
    });
    describe('spawn', () => {
        it('microservice-template', done => {
            const port = Math.floor(Math.random() * 8999) + 1000;
            const query = url.format({ query: {
                    package_name: 'gitlab:omtinez/microservice-template',
                    run_args: JSON.stringify({ port: port })
                } });
            request(service.server).get('/spawn' + query).expect(200).then(res => {
                const pid = JSON.parse(res.text).data.pid;
                assert.notEqual(pid, null);
                const endpoint = url.format({
                    port: port,
                    protocol: 'http',
                    hostname: '127.0.0.1',
                    pathname: '/'
                });
                // Give the microservice a few seconds to start up
                setTimeout(() => {
                    requests.get(endpoint, (err, res) => {
                        assert.equal(err, null);
                        assert.equal(res.body, 'Hello World');
                        done();
                    });
                }, 5000);
            });
        }).timeout(120000);
        it('keep alive', done => {
            const port = Math.floor(Math.random() * 8999) + 1000;
            const query = url.format({ query: {
                    package_name: 'gitlab:omtinez/microservice-template',
                    run_args: JSON.stringify({ port: port })
                } });
            request(service.server).get('/spawn' + query).expect(200).then(res => {
                const pid = JSON.parse(res.text).data.pid;
                const endpoint = url.format({
                    port: port,
                    protocol: 'http',
                    hostname: '127.0.0.1',
                    pathname: '/'
                });
                // Give the microservice a few seconds to start up
                new Promise((resolve, reject) => {
                    setTimeout(resolve, 5000);
                }).then(() => {
                    return new Promise((resolve, reject) => {
                        requests.get(endpoint, (err, res) => {
                            assert.equal(err, null);
                            assert.equal(res.body, 'Hello World');
                            resolve();
                        });
                    });
                }).then(() => {
                    return new Promise((resolve, reject) => {
                        // Kill the microservice
                        console.log('Killing process...');
                        process.kill(pid);
                        // Give the microservice a few seconds to start up
                        console.log('Waiting for process to restart...');
                        setTimeout(resolve, 5000);
                    });
                }).then(() => {
                    // Test the service again
                    requests.get(endpoint, (err, res) => {
                        assert.equal(err, null);
                        assert.equal(res.body, 'Hello World');
                        done();
                    });
                });
            });
        }).timeout(120000);
        it('keep alive not', done => {
            const port = Math.floor(Math.random() * 8999) + 1000;
            const query = url.format({ query: {
                    package_name: 'gitlab:omtinez/microservice-template',
                    run_args: JSON.stringify({ port: port }),
                    keepalive: null
                } });
            request(service.server).get('/spawn' + query).expect(200).then(res => {
                const pid = JSON.parse(res.text).data.pid;
                const endpoint = url.format({
                    port: port,
                    protocol: 'http',
                    hostname: '127.0.0.1',
                    pathname: '/'
                });
                // Give the microservice a few seconds to start up
                new Promise((resolve, reject) => {
                    setTimeout(resolve, 5000);
                }).then(() => {
                    return new Promise((resolve, reject) => {
                        requests.get(endpoint, (err, res) => {
                            assert.equal(err, null);
                            assert.equal(res.body, 'Hello World');
                            resolve();
                        });
                    });
                }).then(() => {
                    return new Promise((resolve, reject) => {
                        // Kill the microservice
                        console.log('Killing process...');
                        process.kill(pid);
                        // Give the microservice a few seconds to start up
                        console.log('Waiting for process to restart...');
                        setTimeout(resolve, 5000);
                    });
                }).then(() => {
                    // Test the service again
                    requests.get(endpoint, (err, res) => {
                        assert.equal(res, null);
                        assert.notEqual(err, null);
                        done();
                    });
                });
            });
        }).timeout(120000);
        it('non-existent module', done => {
            const port = Math.floor(Math.random() * 8999) + 1000;
            const query = url.format({ query: {
                    package_name: 'asdfasdf:asdfasdf',
                    run_args: JSON.stringify({ port: port })
                } });
            request(service.server).get('/spawn' + query).expect(500, done);
        }).timeout(30000);
        it('load at start', done => {
            const port = Math.floor(Math.random() * 8999) + 1000;
            const query = url.format({ query: {
                    package_name: 'gitlab:omtinez/microservice-template',
                    run_args: JSON.stringify({ port: port }),
                    keepalive: false
                } });
            request(service.server).get('/spawn' + query).expect(200).then(res => {
                const pid = JSON.parse(res.text).data.pid;
                const endpoint = url.format({
                    port: port,
                    protocol: 'http',
                    hostname: '127.0.0.1',
                    pathname: '/'
                });
                // Give the microservice a few seconds to start up
                new Promise((resolve, reject) => {
                    setTimeout(resolve, 5000);
                }).then(() => {
                    return new Promise((resolve, reject) => {
                        requests.get(endpoint, (err, res) => {
                            assert.equal(err, null);
                            assert.equal(res.body, 'Hello World');
                            resolve();
                        });
                    });
                }).then(() => {
                    return new Promise((resolve, reject) => {
                        // Kill the microservice
                        console.log('Killing process...');
                        process.kill(pid);
                        service.db.run(`UPDATE spawned SET keepalive = 1 WHERE pid = ?`, [pid]);
                        index_1.main();
                        // Give the microservice a few seconds to start up
                        console.log('Waiting for process to restart...');
                        setTimeout(resolve, 5000);
                    });
                }).then(() => {
                    // Test the service again
                    requests.get(endpoint, (err, res) => {
                        assert.equal(err, null);
                        assert.equal(res.body, 'Hello World');
                        done();
                    });
                });
            });
        }).timeout(120000);
    });
    describe('kill', () => {
        it('keep alive', done => {
            const port = Math.floor(Math.random() * 8999) + 1000;
            const query = url.format({ query: {
                    package_name: 'gitlab:omtinez/microservice-template',
                    run_args: JSON.stringify({ port: port })
                } });
            request(service.server).get('/spawn' + query).expect(200).then(res => {
                const pid = JSON.parse(res.text).data.pid;
                const endpoint = url.format({
                    port: port,
                    protocol: 'http',
                    hostname: '127.0.0.1',
                    pathname: '/'
                });
                // Give the microservice a few seconds to start up
                new Promise((resolve, reject) => {
                    setTimeout(resolve, 5000);
                }).then(() => {
                    return new Promise((resolve, reject) => {
                        requests.get(endpoint, (err, res) => {
                            assert.equal(err, null);
                            assert.equal(res.body, 'Hello World');
                            resolve();
                        });
                    });
                }).then(() => {
                    return new Promise((resolve, reject) => {
                        // Kill the microservice
                        request(service.server).get('/kill?pid=' + pid).expect(200).then(res => {
                            // Give the microservice a few seconds to start up
                            console.log('Waiting for process to restart...');
                            setTimeout(resolve, 5000);
                        });
                    });
                }).then(() => {
                    requests.get(endpoint, (err, res) => {
                        assert.equal(err, null);
                        assert.equal(res.body, 'Hello World');
                        done();
                    });
                });
            });
        }).timeout(120000);
    });
});
