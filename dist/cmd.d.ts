/// <reference types="node" />
import { ChildProcess } from 'child_process';
export declare type SpawnedProcess = {
    proc: ChildProcess;
    module: string;
    pid: number;
    args: string[];
    timestamp: number;
};
export declare function run(bin: string, args: string[]): Promise<void>;
export declare function load(module: string, args: string[], timeout?: number): Promise<SpawnedProcess>;
